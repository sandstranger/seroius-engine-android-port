package engine

import android.content.Context
import android.os.Environment
import android.os.Process
import engine.activity.EngineActivity
import utils.extensions.startActivity
import utils.setEnv

internal val jniLibsArray= arrayOf("ssam")


internal const val MAIN_ENGINE_NATIVE_LIB = "libssam.so"

internal const val SSAM_DATA_PATH_KEY = "SSAM_DATA_PATH";
internal const val SSAM_NATIVE_LIBRARY_DIR = "EXEC_PATH";
internal val SSAM_DATA_PATH = Environment.getExternalStorageDirectory().path + "/ssam/";

private val enableGles2 = {
    val gl4esKey = "LIBGL_ES"
    val glesVersion = "2"

    setEngineEnv(gl4esKey, glesVersion)
}

fun startEngine(context: Context) {
    setGameSearchPath(context)
    enableGles2()
    context.startActivity<EngineActivity>()
}

fun killEngine() = Process.killProcess(Process.myPid())

private fun setGameSearchPath(context: Context) {
    val nativelibraryDir = context.applicationInfo.nativeLibraryDir + "/";

    setEngineEnv(SSAM_DATA_PATH_KEY, SSAM_DATA_PATH)
    setEngineEnv(SSAM_NATIVE_LIBRARY_DIR, nativelibraryDir)
}

private fun setEngineEnv(envKey: String, envValue: String) = setEnv(envKey,envValue)
