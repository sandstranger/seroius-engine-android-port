package engine.activity

import engine.MAIN_ENGINE_NATIVE_LIB
import engine.jniLibsArray
import engine.killEngine
import org.libsdl.app.ESCAPE_KEYCODE
import org.libsdl.app.SDLActivity
import org.libsdl.app.onEscapeBtnClicked

internal class EngineActivity : SDLActivity() {

    override fun getMainSharedObject() = MAIN_ENGINE_NATIVE_LIB

    override fun getLibraries() = jniLibsArray

    override fun onBackPressed() = onEscapeBtnClicked(ESCAPE_KEYCODE)

    override fun onDestroy() {
        super.onDestroy()
        killEngine()
    }
}
