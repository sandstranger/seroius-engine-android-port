package utils

import android.os.Build
import android.system.Os
import libcore.io.Libcore

fun setEnv(name: String, value: String) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        Os.setenv(name, value, true)
    } else {
        Libcore.os.setenv(name, value,true)
    }
}