package ui.fragment

import android.support.v4.app.Fragment
import android.view.View
import com.ssam.R
import kotlinx.android.synthetic.main.main_fragment.*
import com.arellomobile.mvp.presenter.InjectPresenter
import interfaces.MainFragmentView
import presenter.MainFragmentPresenter
import single.fragment.SingleFragment


internal class MainFragment : SingleFragment(), MainFragmentView {

    @InjectPresenter
    lateinit var presenter: MainFragmentPresenter

    override fun layoutResourceId(): Int = R.layout.main_fragment

    override fun bindView(view: View) {
        startGameButton.setOnClickListener {
            presenter.onStartGameBtnClicked(context!!)
        }
    }

    companion object {
        fun newInstance(): Fragment = MainFragment()
    }
}