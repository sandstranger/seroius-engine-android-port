package ui.activity

import android.support.v4.app.Fragment
import single.activity.SingleActivity
import ui.fragment.MainFragment

internal class MainActivity : SingleActivity() {
    override fun getFragment(): Fragment = MainFragment.newInstance()
}
