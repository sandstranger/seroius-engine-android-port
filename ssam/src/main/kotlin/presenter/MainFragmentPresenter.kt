package presenter

import android.content.Context
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import engine.startEngine
import interfaces.MainFragmentView

@InjectViewState
class MainFragmentPresenter : MvpPresenter<MainFragmentView>() {
    fun onStartGameBtnClicked(context: Context) = startEngine(context)
}