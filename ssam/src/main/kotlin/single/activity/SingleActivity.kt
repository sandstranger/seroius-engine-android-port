package single.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.ssam.R

abstract class SingleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single)
        changeFragment()
    }

    protected abstract fun getFragment(): Fragment

    private fun changeFragment() {
        val fragmentContainterResId = R.id.fragments_container
        supportFragmentManager.beginTransaction().apply {
            replace(fragmentContainterResId, getFragment())
            commit()
        }
    }
}
