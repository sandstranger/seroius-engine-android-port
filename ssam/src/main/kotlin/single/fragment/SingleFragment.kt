package single.fragment

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater


abstract class SingleFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(layoutResourceId(), container, false)
        bindView(view)
        return view
    }

    protected abstract fun bindView(view: View)
    @LayoutRes
    protected abstract fun layoutResourceId(): Int
}